# 材料（１人前）

* 玉子　２個
* 塩　少々
* コショウ　少々
 

# 道具

* フライパン[^1]
* ボール
* 泡立て器
[^1]: フライパンの規定については、別途定める

# 手順

* 混ぜる
* 焼く
* 初めは中火、途中から弱火にする

<details><summary>Boostnoteとはmarkdown記法に対応したメモ帳で、情報整理・共有のためのツールです。</summary>
-機能-<br>
・メモを一発で見つける検索機能<br>
・markdown記法に対応<br>
・Mac,Windows,Linux,iOS,Androidに対応<br>
・Plain text(.txt),Markdown(.md)形式にエクスポート、インポート<br>
・PDF保存に対応<br>
・オフラインで使用できる<br>
・設定でdropboxなどに同期可能<br>
・テーマカラーや、数多くのフォントに対応<br>
</details>